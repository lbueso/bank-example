defmodule BankGenServerTest do
  use EQC
  use ExUnit.Case

  test "simple test" do
    BankSup.start_link()
    GenServer.call(BankGenServer, {:new_account})
    GenServer.call(BankGenServer, {:new_account})
    GenServer.call(BankGenServer, {:deposit,  0, 100})
    GenServer.call(BankGenServer, {:deposit,  1, 100})
    GenServer.call(BankGenServer, {:withdraw, 0, 25})
    GenServer.call(BankGenServer, {:withdraw, 1, 25})
    result = GenServer.call(BankGenServer, {:transfer, 0, 1, 25})
    assert result == {:ok, {0, 50}, {1, 100}}
  end

  test "testing deposit with 1 account" do
    BankSup.start_link()
    prop = forall n <- :eqc_gen.nat do
      {:ok, id} = GenServer.call(BankGenServer, {:new_account})
      {:ok, _, balance} = GenServer.call(BankGenServer, {:deposit, id, n})
      balance == n
    end
    assert :eqc.quickcheck(prop)
  end

  test "testing withdraw after deposit with 1 account" do
    BankSup.start_link()

    prop = forall {n, m} <- {:eqc_gen.nat, :eqc_gen.nat} do
      implies n >= m do
        {:ok, id} = GenServer.call(BankGenServer, {:new_account})
        {:ok, _, balance1} = GenServer.call(BankGenServer, {:deposit,  id, n})
        {:ok, _, balance2} = GenServer.call(BankGenServer, {:withdraw, id, m})
        Supervisor.terminate_child(BankSup, BankGenServer)
        Supervisor.restart_child(BankSup, BankGenServer)
        balance1 == n and balance2 == n - m
      end
    end
    assert :eqc.quickcheck(prop)
  end

  test "testing transfer after deposit wit 2 accounts" do
    BankSup.start_link()

    prop = forall {n, m} <- {:eqc_gen.nat, :eqc_gen.nat} do
      {:ok, id1} = GenServer.call(BankGenServer, {:new_account})
      {:ok, id2} = GenServer.call(BankGenServer, {:new_account})
      {:ok, _, _} = GenServer.call(BankGenServer, {:deposit, id1, n})
      {:ok, _, _} = GenServer.call(BankGenServer, {:deposit, id2, m})
      forall {t, from, to} <- {:eqc_gen.nat, :eqc_gen.oneof([id1, id2]), :eqc_gen.oneof([id1, id2])} do
        implies from != to do
          {:ok, {_, from_balance}, {_, to_balance}} = GenServer.call(BankGenServer, {:transfer, from, to, t})
          Supervisor.terminate_child(BankSup, BankGenServer)
          Supervisor.restart_child(BankSup, BankGenServer)
          from_balance + to_balance == n + m
        end
      end
    end
    assert :eqc.quickcheck(prop)
  end
end

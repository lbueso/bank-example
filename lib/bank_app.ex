defmodule BankApp do
  use Application

  def start(_type, _args) do
    IO.puts("Starting the app...") # show that the app is really starting
    BankSup.start_link()
  end

  def test() do
    GenServer.call(BankServer, {:create_account})
    GenServer.call(BankServer, {:create_account})
    GenServer.call(BankServer, {:deposit,  0, 100})
    GenServer.call(BankServer, {:deposit,  1, 100})
    GenServer.call(BankServer, {:withdraw, 0, 25})
    GenServer.call(BankServer, {:withdraw, 1, 25})
    GenServer.call(BankServer, {:transfer, 0, 1, 25})
  end

end

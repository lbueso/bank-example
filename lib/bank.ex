defmodule State do
  defstruct id: 0, accounts: %{}
end

defmodule Bank do

  @moduledoc """
  This module contains the logic of a banking program.
  """

  @doc """
  Computes the next state.
  """

  def next({:new_account}, %State{id: id, accounts: accounts}) do
    %State{ id: id + 1, accounts: Map.put_new(accounts, id, 0) }
  end

  def next({operation, account, amount}, state = %State{accounts: accounts}) do
    %{ ^account => balance } = accounts
    new_balance = case operation do
                    :deposit  -> balance + amount
                    :withdraw -> balance - amount
                  end
    %State{state | accounts: %{ accounts | account => new_balance }}
  end

  def next({:transfer, from, to, amount}, state = %State{accounts: accounts}) do
    %{ ^from => from_balance,
       ^to   => to_balance
    } = accounts
    %State{ state | accounts:
            %{accounts | from => from_balance - amount,
              to   => to_balance   + amount
            } }
  end

  def next(_, state), do: state

  @doc """
  Generates the result of a given operation, should be called after computing
  the next state.
  """

  def return({:new_account}, %State{id: id}) do
    {:ok, id - 1}
  end

  def return({_, account, _}, %State{accounts: accounts}) do
    %{ ^account => balance } = accounts
    {:ok, account, balance}
  end

  def return({_, from, to, _}, %State{accounts: accounts}) do
    %{ ^from => from_balance,
       ^to   => to_balance
    } = accounts
    {:ok, {from, from_balance}, {to, to_balance}}
  end

  def return(op, _) do
    IO.puts(inspect op)
    {:error, "unknown operation"}
  end

end

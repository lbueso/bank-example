defmodule BankEqcStatem do
  use EQC
  use EQC.StateM

  ##############################################################################

  defmodule StateM do
    defstruct id: 0, accounts: %{}, initialized: false

    def initialized?(%StateM{ initialized: v }), do: v

    def initialize(state), do: %StateM{ state | initialized: true }

    def empty(), do: %StateM{}

    def get_accounts(%StateM{ accounts: accounts }), do: accounts

    def get_accounts_list(state), do: Map.keys(get_accounts(state))

    def get_number_of_accounts(state), do: length(get_accounts_list(state))

    def create_account(%StateM{ id: id, accounts: accounts }),
      do: %StateM{ id: id + 1, accounts: Map.put_new(accounts, id, 0) }

    def get_id(%StateM{id: id}), do: id

    def get_balance(%StateM{ accounts: accounts }, account) do
      %{ ^account => balance } = accounts
      balance
    end

    def put_account(state = %StateM{ accounts: accounts }, account, balance) do
      if account in Map.keys(accounts) do
        %StateM{ state | accounts: %{ accounts | account => balance  } }
      else
        %StateM{ state | accounts: Map.put_new(accounts, account, balance ) }
      end
    end
  end

  ##############################################################################

  def initial_state(), do: StateM.empty()

  ##############################################################################

  def start_pre(state), do: not StateM.initialized?(state)

  def start_args(_state), do: []

  def start() do
    BankSup.start_link()
    Supervisor.terminate_child(BankSup, BankGenServer)
    Supervisor.restart_child(BankSup, BankGenServer)
  end

  def start_next(_state, _var, _args), do: StateM.initialize(StateM.empty())

  ##############################################################################

  def new_account_pre(state), do: StateM.initialized?(state)

  def new_account_args(_state), do: []

  def new_account(),
    do: GenServer.call(BankGenServer, {:new_account})

  def new_account_post(state, _args, result),
    do: StateM.get_id(state) in Tuple.to_list(result)

  def new_account_next(state, _var, _args),
    do: StateM.create_account(state)

  ##############################################################################

  def deposit_pre(state), do: StateM.get_number_of_accounts(state) > 0

  def deposit_args(state),
    do: let account <- :eqc_gen.oneof(StateM.get_accounts_list(state)),
    do: let amount  <- :eqc_gen.nat(),
    do: [account, amount]

  def deposit(account, amount),
    do: GenServer.call(BankGenServer, {:deposit, account, amount})

  def deposit_post(state, [account, amount], result),
    do: StateM.get_balance(state, account) + amount in Tuple.to_list(result)


  def deposit_next(state, _var, [account, amount]) do
    balance = StateM.get_balance(state, account)
    StateM.put_account(state, account, amount + balance)
  end

  ##############################################################################

  def withdraw_pre(state), do: StateM.get_number_of_accounts(state) > 0

  def withdraw_args(state),
    do: let account <- :eqc_gen.oneof(StateM.get_accounts_list(state)),
    do: let amount  <- :eqc_gen.nat(),
    do: [account, amount]

  def withdraw(account, amount),
    do: GenServer.call(BankGenServer, {:withdraw, account, amount})

  def withdraw_post(state, [account, amount], result),
    do: StateM.get_balance(state, account) - amount in Tuple.to_list(result)

  def withdraw_next(state, _var, [account, amount]) do
    balance = StateM.get_balance(state, account)
    StateM.put_account(state, account, balance - amount)
  end

  ##############################################################################

  def transfer_pre(state), do: StateM.get_number_of_accounts(state) > 2

  def transfer_args(state),
    do: let from   <- :eqc_gen.oneof(StateM.get_accounts_list(state)),
    do: let to     <- :eqc_gen.oneof(StateM.get_accounts_list(state)),
    do: let amount <- :eqc_gen.nat(),
    do: [from, to, amount]

  def transfer(from, to, amount),
    do: GenServer.call(BankGenServer, {:transfer, from, to, amount})

  def transfer_post(state, [from, to, amount], {:ok, {_, from_result}, {_, to_result}}) do
    from_balance = StateM.get_balance(state, from)
    to_balance   = StateM.get_balance(state, to)
    from_balance - amount == from_result and to_balance + amount == to_result
  end

  def transfer_next(state, _var, [from, to, amount]) do
    from_balance = StateM.get_balance(state, from)
    to_balance   = StateM.get_balance(state, to)
    StateM.put_account(state, from, from_balance - amount)
    |> StateM.put_account(to, to_balance + amount)
  end

  ##############################################################################

  def sample(),
    do: commands(__MODULE__) |> sample

  def prop() do
    forall cmds <- commands(__MODULE__) do
      result_list = run_commands(cmds)
      result = Keyword.get(result_list, :result)
      pretty_commands(cmds, result_list, result == :ok)
    end
  end

  def eqc(), do: :eqc.quickcheck(prop())

  ##############################################################################

end

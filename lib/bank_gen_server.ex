defmodule BankGenServer do
  use GenServer

  # This is a convenience method for startup
  def start_link do
    GenServer.start_link(__MODULE__, [], [{:name, __MODULE__}])
  end

  # These are the callbacks that the GenServer behaviour will use
  def init([]) do
    {:ok, %State{}}
  end

  def handle_call(operation, _from, state) do
    new_state = Bank.next(operation, state)
    reply = Bank.return(operation, new_state)
    {:reply, reply, new_state}
  end

  def handle_cast(_msg, state) do
    {:noreply, state}
  end

  def handle_info(_info, state) do
    {:noreply, state}
  end

  def terminate(_reason, _state) do
    {:ok}
  end

  def code_change(_old_version, state, _extra) do
    {:ok, state}
  end

end
